M06-ASO LDAP SERVER BASE
Escola Del Treball
2HISX 2020-2021
Carles Grilló Oller isx23921734

carlesgrillo/ldap:practica Afegir un nou objectClass i atributs, master.schema

Exemples d'atributs que té el master:

    Aprovat Atribut que indica si l'alumne ha aprovat el master, es un boolean.

    Especialitat Atribut on s'especifica de quina especialitat es el master cursat.

    Nota Atribut numeric on s'indica la nota final del alumne.

    Web Atribut amb una URL de la seva pàgina web. Pot ser multi-valued.

    Foto Atribut amb una foto JPEG del alumne. Pot ser multi-value.

    Extra es un atribut binari per contenir documents del alumne.

